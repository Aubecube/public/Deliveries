package fr.minepod.deliveries;

import java.util.Iterator;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import lombok.RequiredArgsConstructor;
import net.citizensnpcs.api.event.NPCRightClickEvent;

@RequiredArgsConstructor
public class DeliveriesListener implements Listener {
  private final Deliveries plugin;

  @EventHandler
  public void onNPCRightClick(NPCRightClickEvent event) {
    if (!event.isCancelled()) {
      for (String npc : plugin.config.getNPCs()) {
        if (npc.equals(String.valueOf(event.getNPC().getId()))) {
          event.setCancelled(true);
          event.getClicker().openInventory(
              plugin.createInventory(event.getClicker().getUniqueId(), Deliveries.INVENTORY_NAME));

          return;
        }
      }
    }
  }

  @EventHandler
  public void onInventoryClick(InventoryClickEvent event) {
    if (event.getView().getTitle().startsWith(Deliveries.INVENTORY_NAME)
        && event.getRawSlot() < Deliveries.INVENTORY_SIZE + 9) {
      Player player = (Player) event.getWhoClicked();

      ItemStack current = event.getCurrentItem();
      if (current != null && current.getType() != Material.AIR) {
        if (current.getItemMeta().getDisplayName() == NavigationOptions.NEXT.getDisplay()) {
          event.getWhoClicked()
              .openInventory(plugin.createInventory(event.getWhoClicked().getUniqueId(),
                  Deliveries.INVENTORY_NAME,
                  plugin.pages.get(event.getWhoClicked().getUniqueId()) + 1));
        } else if (current.getItemMeta().getDisplayName() == NavigationOptions.PREVIOUS
            .getDisplay()) {
          event.getWhoClicked()
              .openInventory(plugin.createInventory(event.getWhoClicked().getUniqueId(),
                  Deliveries.INVENTORY_NAME,
                  plugin.pages.get(event.getWhoClicked().getUniqueId()) - 1));
        } else if (event.getRawSlot() < Deliveries.INVENTORY_SIZE) {
          List<ItemStack> items = plugin.players.getFor(player.getUniqueId());
          if (items != null && items.size() > 0) {
            if (player.getInventory().firstEmpty() != -1) {
              if (items.remove(current)) {
                if (current.hasItemMeta()) {
                  ItemMeta meta = current.getItemMeta();

                  List<String> lore = meta.getLore();
                  if (lore != null) {
                    for (Iterator<String> i = lore.iterator(); i.hasNext();) {
                      String line = i.next();

                      if (line.startsWith(Deliveries.REMOVE_LINE_TAG)) {
                        i.remove();
                      }
                    }

                    meta.setLore(lore);
                    current.setItemMeta(meta);
                  }
                }

                player.getInventory().addItem(current);
                event.getInventory().setItem(event.getRawSlot(), new ItemStack(Material.AIR));

                plugin.messages.sendFormatted(player, ChatColor.GREEN + "Tu as reçu l'objet.");
              } else {
                plugin.messages.sendFormatted(player,
                    ChatColor.RED + "Le facteur a eu un problème.");
              }
            } else {
              plugin.messages.sendFormatted(player,
                  ChatColor.RED + "Tu n'as pas de place dans ton inventaire.");
            }
          }
        }
      }

      event.setCancelled(true);
    }
  }

  @EventHandler
  public void onInventoryDrag(InventoryDragEvent event) {
    if (event.getView().getTitle().startsWith(Deliveries.INVENTORY_NAME)) {
      for (int slot : event.getRawSlots()) {
        if (slot < Deliveries.INVENTORY_SIZE + 9) {
          event.setCancelled(true);

          return;
        }
      }
    }
  }

  @EventHandler
  public void onPlayerJoin(PlayerJoinEvent event) {
    plugin.notify(event.getPlayer());
  }

  @EventHandler
  public void onPlayerQuit(PlayerQuitEvent event) {
    plugin.pages.remove(event.getPlayer().getUniqueId());
  }

}
