package fr.minepod.deliveries;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import fr.minepod.deliveries.data.ConfigDataManager;
import fr.minepod.deliveries.data.PlayersDataManager;
import fr.minepod.mpcore.APIVersion;
import fr.minepod.mpcore.MPCoreDepends;
import fr.minepod.mpcore.api.classes.PlayerClass;
import fr.minepod.mpcore.api.commands.CommandBase;
import fr.minepod.mpcore.api.commands.CommandRegistry;
import fr.minepod.mpcore.api.commands.CommandType;
import fr.minepod.mpcore.api.data.AutoDataManager;
import fr.minepod.mpcore.api.data.DataPolicy;
import fr.minepod.mpcore.api.messages.MessagesManager;
import fr.minepod.mpcore.api.uuids.UUIDsManager;
import fr.minepod.mpcore.data.proxies.DataConfigManagerProxy;
import fr.minepod.mpcore.utils.WrapUtils;

public class Deliveries extends JavaPlugin implements MPCoreDepends {
  @Override
  public APIVersion getAPIVersion() {
    return APIVersion.ENGELBERG;
  }

  public MessagesManager messages = new MessagesManager("Deliveries");

  public ConfigDataManager config;
  public PlayersDataManager players = new PlayersDataManager(this, "players.yml");

  public CommandRegistry<Deliveries> commands = new CommandRegistry<>(this, "de");

  public final static String INVENTORY_NAME = ChatColor.YELLOW + "Facteur";
  public final static int INVENTORY_SIZE = 45;
  public final static String REMOVE_LINE_TAG = "§6§c§6§a§6§d§6§6§o§n§r";

  public Map<UUID, Integer> pages = new HashMap<>();

  public void onEnable() {
    getLogger().info("Enabling...");

    try {
      config = DataConfigManagerProxy.addProxy(
          new ConfigDataManager(this, "config.yml", DataPolicy.CONFIG), ConfigDataManager.class);
    } catch (Exception e) {
      e.printStackTrace();
    }

    AutoDataManager.onEnable(this);

    commands.put("version", new CommandBase("Voir la version du plugin", "version",
        CommandType.USER, (sender, command, label, args, processed) -> {
          messages.sendFormatted(sender, "Version " + getDescription().getVersion());
        }));

    commands.put("deliver", new CommandBase("Livrer un item pour un joueur",
        "deliver [string=joueur] [string=material] [int=data] [int=quantité] [string=nom/none] [chain=lore/none]",
        CommandType.ADMINISTRATOR, (sender, command, label, args, processed) -> {
          PlayerClass player = UUIDsManager.getPlayerClass(processed.get(1).getStringValue());
          if (player != null) {
            Material material = null;
            try {
              material = Material.valueOf(processed.get(2).getStringValue().toUpperCase());
            } catch (Exception ignored) {
              messages.sendFormatted(sender, ChatColor.RED + "Le material " + ChatColor.YELLOW
                  + processed.get(2).getStringValue() + ChatColor.RED + " n'existe pas.");
              return;
            }

            short data = (short) processed.get(3).getIntValue();
            String name = processed.get(5).getStringValue().replace("&", "§");
            String chain = processed.get(6).getStringValue().replace("&", "§");

            List<String> lines = new ArrayList<>();
            if (!chain.equalsIgnoreCase("none")) {
              String[] parts = chain.split("\\|");
              for (String part : parts) {
                for (String line : WrapUtils.wrapString(part, '§', 50, false)) {
                  lines.add(ChatColor.RESET + line);
                }
              }
            }

            List<ItemStack> items = players.getFor(player.getUUID());
            if (items == null) {
              items = new ArrayList<>();
            }

            int total = processed.get(4).getIntValue();
            do {
              int amount = Math.min(total, material.getMaxStackSize());
              total -= amount;

              ItemStack item = new ItemStack(material, amount, data);

              ItemMeta meta = item.getItemMeta();

              if (!name.equalsIgnoreCase("none")) {
                meta.setDisplayName(name);
              }

              meta.setLore(lines);

              item.setItemMeta(meta);

              items.add(item);
            } while (total > 0);

            players.setFor(player.getUUID(), items);

            messages.sendFormatted(sender, ChatColor.GREEN + "L'objet a été livré.");

            for (Player online : getServer().getOnlinePlayers()) {
              if (online.getUniqueId().equals(player.getUUID())) {
                notify(online);

                messages.sendFormatted(sender, ChatColor.GREEN + "Le joueur a été notifié.");
                return;
              }
            }
          } else {
            messages.sendFormatted(sender, ChatColor.RED + "Le joueur " + ChatColor.YELLOW
                + processed.get(1).getStringValue() + ChatColor.RED + " n'existe pas.");
          }
        }));

    commands.put("clear",
        new CommandBase("Nettoyer les livraisons d'un joueur", "clear [string=joueur]",
            CommandType.ADMINISTRATOR, (sender, command, label, args, processed) -> {
              PlayerClass player = UUIDsManager.getPlayerClass(processed.get(1).getStringValue());
              if (player != null) {
                players.removeFor(player.getUUID());

                messages.sendFormatted(sender,
                    ChatColor.GREEN + "Les livraisons du joueur " + ChatColor.YELLOW
                        + processed.get(1).getStringValue() + ChatColor.GREEN
                        + " ont été nettoyées.");
              } else {
                messages.sendFormatted(sender, ChatColor.RED + "Le joueur " + ChatColor.YELLOW
                    + processed.get(1).getStringValue() + ChatColor.RED + " n'existe pas.");
              }
            }));

    commands.put("open",
        new CommandBase("Ouvrir les livraisons d'un joueur (lecture seulement)",
            "open [string=joueur]", CommandType.ADMINISTRATOR,
            (sender, command, label, args, processed) -> {
              if (sender instanceof Player) {
                PlayerClass player = UUIDsManager.getPlayerClass(processed.get(1).getStringValue());
                if (player != null) {
                  Player opener = (Player) sender;

                  opener.openInventory(
                      createInventory(player.getUUID(), ChatColor.YELLOW + player.getName()));
                } else {
                  messages.sendFormatted(sender, ChatColor.RED + "Le joueur " + ChatColor.YELLOW
                      + processed.get(1).getStringValue() + ChatColor.RED + " n'existe pas.");
                }
              } else {
                messages.sendFormatted(sender,
                    ChatColor.RED + "Commande exécutable uniquement par un joueur.");
              }
            }));

    commands.put("reload", new CommandBase("Recharger la configuration", "reload",
        CommandType.ADMINISTRATOR, (sender, command, label, args, processed) -> {
          config.clear();
          config.load();

          messages.sendFormatted(sender, ChatColor.GREEN + "La configuration a été rechargée.");
        }));

    commands.put("save", new CommandBase("Sauvegarder la configuration", "save",
        CommandType.ADMINISTRATOR, (sender, command, label, args, processed) -> {
          try {
            players.save();

            messages.sendFormatted(sender, ChatColor.GREEN + "La configuration a été sauvegardée.");
          } catch (IOException e) {
            e.printStackTrace();

            messages.sendFormatted(sender,
                ChatColor.RED + "La configuration n'a pas pu être sauvegardée.");
          }
        }));

    getServer().getPluginManager().registerEvents(new DeliveriesListener(this), this);
    getCommand("deliveries").setExecutor(commands);

    getLogger().info("Enabled!");
  }

  public void onDisable() {
    getLogger().info("Disabling...");

    AutoDataManager.onDisable(this);

    getLogger().info("Disabled!");
  }

  public Inventory createInventory(UUID uuid, String name) {
    return createInventory(uuid, name, 0);
  }

  public Inventory createInventory(UUID uuid, String name, int page) {
    Inventory inventory;

    ItemStack filler = new ItemStack(Material.CYAN_STAINED_GLASS_PANE, 1);

    ItemMeta fillerMeta = filler.getItemMeta();
    fillerMeta.setDisplayName("§6-");
    filler.setItemMeta(fillerMeta);

    List<ItemStack> items = players.getFor(uuid);
    if (items != null && items.size() > 0) {
      int start = Math.max(INVENTORY_SIZE * page, 0);
      int end = Math.min(start + INVENTORY_SIZE, items.size());

      page = (int) Math.ceil((double) start / (double) INVENTORY_SIZE);

      inventory =
          Bukkit.createInventory(null, INVENTORY_SIZE + 9, name + " » §6page " + (page + 1));

      if (items.size() > start) {
        int position = 0;
        for (int i = start; i < end; i++) {
          inventory.setItem(position, items.get(i));

          position++;
        }
      }

      if (start > 0) {
        ItemStack item = new ItemStack(Material.ARROW, 1);
        ItemMeta meta = item.getItemMeta();

        meta.setDisplayName(NavigationOptions.PREVIOUS.getDisplay());
        item.setItemMeta(meta);

        inventory.setItem(45, item);
      } else {
        inventory.setItem(45, filler);
      }

      if (end < items.size()) {
        ItemStack item = new ItemStack(Material.ARROW, 1);
        ItemMeta meta = item.getItemMeta();

        meta.setDisplayName(NavigationOptions.NEXT.getDisplay());
        item.setItemMeta(meta);

        inventory.setItem(53, item);
      } else {
        inventory.setItem(53, filler);
      }

      for (int i = 46; i < 53; i++) {
        inventory.setItem(i, filler);
      }
    } else {
      inventory = Bukkit.createInventory(null, INVENTORY_SIZE + 9, name + " » §6page 1");

      for (int i = 45; i < 54; i++) {
        inventory.setItem(i, filler);
      }
    }

    pages.put(uuid, page);

    return inventory;
  }

  public void notify(Player player) {
    List<ItemStack> items = players.getFor(player.getUniqueId());
    if (items != null && items.size() > 0) {
      player.sendMessage(ChatColor.DARK_AQUA + "Tu as reçu " + ChatColor.YELLOW + items.size()
          + " objet(s)" + ChatColor.DARK_AQUA + ". Va voir un " + ChatColor.YELLOW + "PNJ facteur"
          + ChatColor.DARK_AQUA + ".");
    }
  }

  public void deliver(UUID uuid, String lore, ItemStack... add) {
    List<ItemStack> items = players.getFor(uuid);
    if (items == null) {
      items = new ArrayList<>();
    }

    for (ItemStack item : add) {
      if (item != null && item.getType() != Material.AIR) {
        ItemMeta meta = item.getItemMeta();

        if (!lore.isEmpty()) {
          List<String> lines = meta.getLore();
          if (lines == null) {
            lines = new ArrayList<>();
          }

          lines.add(REMOVE_LINE_TAG + lore);

          meta.setLore(lines);
          item.setItemMeta(meta);
        }

        items.add(item);
      }
    }

    players.setFor(uuid, items);

    OfflinePlayer player = getServer().getOfflinePlayer(uuid);
    if (player.isOnline()) {
      notify((Player) player);
    }
  }
}
