package fr.minepod.deliveries.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import fr.minepod.mpcore.api.data.AbstractDataManager;
import fr.minepod.mpcore.api.data.DataPolicy;

public class PlayersDataManager extends AbstractDataManager<UUID, List<ItemStack>> {
  public PlayersDataManager(JavaPlugin plugin, String fileName) {
    super(plugin, fileName, DataPolicy.REGULAR);
  }

  @Override
  public void get() {
    Map<UUID, List<ItemStack>> data = new HashMap<>();

    if (getConfig().isSet("data")) {
      for (String key : getConfig().getConfigurationSection("data").getKeys(false)) {
        List<ItemStack> items = new ArrayList<>();
        for (String child : getConfig().getConfigurationSection("data" + "." + key)
            .getKeys(false)) {
          items.add(getConfig().getItemStack("data." + key + "." + child));
        }

        data.put(UUID.fromString(key), items);
      }
    }

    setData(data);
  }

  @Override
  public void set() {
    getConfig().set("data", null);

    for (Entry<UUID, List<ItemStack>> entry : getData().entrySet()) {
      int i = 0;
      for (ItemStack item : entry.getValue()) {
        getConfig().set("data." + entry.getKey().toString() + "." + i, item);
        i++;
      }
    }
  }
}
