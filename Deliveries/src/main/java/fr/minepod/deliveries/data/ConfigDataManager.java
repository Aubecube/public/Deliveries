package fr.minepod.deliveries.data;

import java.util.ArrayList;
import java.util.List;

import fr.minepod.deliveries.Deliveries;
import fr.minepod.mpcore.api.data.DataPolicy;
import fr.minepod.mpcore.api.data.configs.AbstractConfigDataManager;
import fr.minepod.mpcore.api.data.configs.ConfigDataLoader;
import fr.minepod.mpcore.api.data.configs.ConfigDataLoaderType;

public class ConfigDataManager extends AbstractConfigDataManager<Deliveries, String, Object> {
  /**
   * 
   */
  private static final long serialVersionUID = 132721416885684513L;

  public ConfigDataManager(Deliveries plugin, String file, DataPolicy type) {
    super(plugin, file, type);
  }

  @ConfigDataLoader(path = "npcs", type = ConfigDataLoaderType.LIST)
  public List<String> getNPCs() {
    List<String> list = new ArrayList<>();

    list.add("10");
    list.add("16");

    return list;
  }

  @Override
  public void get() {}

  @Override
  public void set() {
    for (Entry<String, Object> entry : entrySet()) {
      getConfig().set(entry.getKey(), entry.getValue());
    }
  }
}
