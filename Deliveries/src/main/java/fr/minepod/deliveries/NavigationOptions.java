package fr.minepod.deliveries;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum NavigationOptions {
  PREVIOUS("§6Page précédente"), NEXT("§6Page suivante");

  private final String display;
}
